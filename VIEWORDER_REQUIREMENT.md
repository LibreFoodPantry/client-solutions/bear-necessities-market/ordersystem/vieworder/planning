# Requirement #

## Title: View Order

## Pre-Checklist

The "View Orders to be filled" requirement is related to this requirement.

## Stories

Story 1:  As an Admin/Staff, I want to see the details of the order, such as contents of the order, dietary restrictions, OrderID, and other preferences.

Story 2: As an Admin/Staff, I want to see who placed the order via their Student/Customer ID.

## Ready Checklist

| Story 1: As an Admin/Staff, I want to see the details of the order, such as contents of the order, dietary restrictions, OrderID, and other preferences.  | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | No |
| Testable (has testable acceptance criteria) | Done |
| The roles that benefit from this issue are labeled (e.g., role:\*). | No |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

| Story 2: As an Admin/Staff, I want to see who placed the order via their Student/Customer ID.  | Status |
| ------ | ------ |
| Independent of other issues being worked on | No |
| Negotiable (and negotiated) | Done |
| Valuable (the value to an identified role has been identified) | Done |
| Estimable (the size of this story has been estimated) | Done |
| Small (can be completed in 50% or less of a single iteration) | No |
| Testable (has testable acceptance criteria) | Done |
| The roles that benefit from this issue are labeled (e.g., role:\*). | No |
| The related activity in the story map has been identified (e.g., activity:\*). This should be an activity in the Story Map.  | Done |

## Diagrams
Diagram for Mockup 1
![](diagrams/Mockup1.PNG)

Diagram for Mockup 2
![](diagrams/Mockup2.PNG)


This Diagram section will be updated as an appropriate look for the ViewOrder feature is found.
## Acceptance Criteria

### Scenario : Admin/Staff wants to approve an order
Given : They need to view the orders to see if it adheres to guidelines.  
When : The Admin/Staff clicks to view the appropriate order.  
Then : Based on the info from the View page, the Admin/Staff can determine whether or not the order is approved.  

### Scenario : Admin/Staff wants to view an order
Given : They need to view the orders to see if it adheres to guidelines.   
When : The Admin/Staff clicks to view the appropriate order.  
Then : Based on the info from the View page, the Admin/Staff obtains the information they wanted to check.  

## Related Issues

There are currently no related issues.

## Estimate

Story 1: As an Admin/Staff, I want to see the details of the order, such as contents of the order, dietary restrictions, OrderID, and other preferences.: 
Estimate: 8

Story 2: As an Admin/Staff, I want to see who placed the order via their Student/Customer ID.
Estimate: 8


