# Introduction
This document is intended to assist developers as they maintain and edit the ViewOrder component of the BMN
project. Contained in this document will be links to helpful troubleshooting guides for numerous issues/blockers
one may run into while working on the ViewOrder project. The purpose of this document is to make access to 
these resources much faster and therefore save developers value time during sprints from searching Gitlab documentation
or outside sources. 

# CI/CD Resources
- Container Registry: https://docs.gitlab.com/ee/user/packages/container_registry/index.html#troubleshooting-the-gitlab-container-registry
- Docker Build: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#troubleshooting
- Environments: https://docs.gitlab.com/ee/ci/environments/deployment_safety.html#ensure-only-one-deployment-job-runs-at-a-time 
- Merge Trains: https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/index.html#troubleshooting
- Gitlab Runner: https://docs.gitlab.com/runner/faq/


# Rest API Resources
- Project Resources: https://docs.gitlab.com/ee/api/api_resources.html#project-resources
- Group Resources: https://docs.gitlab.com/ee/api/api_resources.html#group-resources
- Standalone Resources: https://docs.gitlab.com/ee/api/api_resources.html#standalone-resources 
- Templates API Resources: https://docs.gitlab.com/ee/api/api_resources.html#templates-api-resources


# Frontend Resources
- Writing End-to-End tests: https://docs.gitlab.com/ee/development/testing_guide/end_to_end/beginners_guide.html 
- Frontend FAQ: https://docs.gitlab.com/ee/development/fe_guide/frontend_faq.html 


# Testing and Additional Resources:
- DAST (Dynamic Application Security Testing) Troubleshooting: https://docs.gitlab.com/ee/user/application_security/dast/dast_troubleshooting.html
- Frontend General/Cannot find Issue: https://docs.gitlab.com/ee/development/fe_guide/troubleshooting.html#this-guide-doesnt-contain-the-issue-i-ran-into
- Auto DevOps: https://docs.gitlab.com/ee/topics/autodevops/troubleshooting.html#detected-an-existing-postgresql-database 


